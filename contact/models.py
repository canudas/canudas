from django.db import models
from django.shortcuts import render

from wagtail.admin.panels import FieldPanel
from wagtail.fields import RichTextField
from wagtail.models import Page

from .utils import send_contact_email, add_to_mailchimp_list


class ContactPage(Page):
    intro = RichTextField(blank=True)
    thank_you_text = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full"),
        FieldPanel('thank_you_text', classname="full"),
    ]

    def serve(self, request):
        from .forms import ContactForm

        if request.method == 'POST':
            form = ContactForm(request.POST)
            if form.is_valid():
                contact_entry = form.save()
                send_contact_email(contact_entry)
                if contact_entry.subscribe_newsletter:
                    add_to_mailchimp_list(contact_entry.email)
                return render(request, 'contact/contact_thanks.html', {
                    'page': self,
                    'thank_you_text': self.thank_you_text,
                })
        else:
            form = ContactForm()

        return render(request, 'contact/contact_form.html', {
            'page': self,
            'form': form,
        })
        return self.email


class ContactFormEntry(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField()
    message = models.TextField(blank=True)
    subscribe_newsletter = models.BooleanField(default=False, verbose_name="Subscribe me to the newsletter")
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.name} - {self.email}"

