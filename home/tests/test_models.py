from django.test import TestCase
from django.utils import timezone
from django.utils import timezone
from wagtail.models import Page
from home.models import HomePage

class HomePageModelTest(TestCase):

    def setUp(self):
        # Set up non-modified objects used by all test methods
        self.root = Page.objects.get(id=1)
        self.home_page = HomePage(title="Home", slug="home-{}".format(timezone.now().strftime("%Y%m%d%H%M%S")))

    def test_instance(self):
        # Test the HomePage instance and its properties
        self.assertTrue(isinstance(self.home_page, HomePage))
        self.assertEqual(self.home_page.title, "Home")

    def test_get_context(self):
        # Test the get_context method
        request = self.client.get(self.home_page.url)
        context = self.home_page.get_context(request)
        self.assertIn('page_tree', context)
        self.assertTrue(context['page_tree'].filter(id=self.home_page.id).exists())
