from django.db import models

from wagtail.admin.panels import FieldPanel, InlinePanel
from wagtail.fields import RichTextField
from wagtail.images.models import Image
from wagtail.models import Page, Orderable
from modelcluster.fields import ParentalKey

class HomePage(Page):
    tagline = models.CharField(blank=True, null=True, default="🛩 Asociación aeronáutica", max_length=50)
    intro = RichTextField(blank=True, null=True, default="<p>Siguiendo los pasos de nuestros pioneros, el Aeroclub Canudas es una entidad sin ánimo de lucro, registrada en el Departamento de Justicia de la Generalitat de Catalunya, con tres objetivos muy bien definidos:</p>")
    intro_1_title = models.CharField(blank=True, null=True, default="Conservar i difundir la historia aeronáutica", max_length=50)
    intro_1_desc = models.CharField(blank=True, null=True, default="del nuestro país (conocer el pasado, entender el presente i crear un futuro mejor).", max_length=100)
    intro_2_title = models.CharField(blank=True, null=True, default="Acercar la aviación al público", max_length=50)
    intro_2_desc = models.CharField(blank=True, null=True, default="así como fomentar su uso i dinamizar el sector.", max_length=100)
    intro_3_title = models.CharField(blank=True, null=True, default="Dar soporte a su colectivo", max_length=50)
    intro_3_desc = models.CharField(blank=True, null=True, default="y mejorar la seguridad.", max_length=100)
    
    news_title = models.CharField(blank=True, null=True, default="Creació de l'aeroclub", max_length=50)
    news_opening = models.CharField(blank=True, null=True, default='Estimados amigos y amigas de la <span class="py-2 inline-block relative before:absolute before:bottom-2.5 before:left-0 before:w-full before:h-3 before:bg-titlebg dark:before:bg-titlebgdark before:-z-1">aviación,</span>', max_length=300)
    news_body = RichTextField(blank=True, null=True, default='')

    historia_title = models.CharField(blank=True, null=True, default='Josep <span class="inline-block relative before:absolute before:bottom-2.5 before:left-0 before:w-full before:h-3 before:bg-titlebg2 dark:before:bg-titlebgdark before:-z-1">Canudas</span> y Busquets', max_length=300)
    historia_body = RichTextField(blank=True, null=True, default='')
    historia_image = models.ForeignKey(
        'wagtailimages.Image', blank=True, null=True, on_delete=models.SET_NULL, related_name='+'
    )

    content_panels = Page.content_panels + [
        FieldPanel('tagline'),
        FieldPanel('intro'),
        FieldPanel('intro_1_title'),
        FieldPanel('intro_1_desc'),
        FieldPanel('intro_2_title'),
        FieldPanel('intro_2_desc'),
        FieldPanel('intro_3_title'),
        FieldPanel('intro_3_desc'),
        InlinePanel('carousel_images', label="Carousel images"),
        FieldPanel('news_title'),
        FieldPanel('news_opening'),
        FieldPanel('news_body'),
        InlinePanel('sections',
            heading="Sections",
            label="Section",
        ),
        FieldPanel('historia_title'),
        FieldPanel('historia_body'),
        FieldPanel('historia_image'),
    ]

    def get_context(self, request):
        context = super().get_context(request)
        
        context['carousel_images'] = self.carousel_images.all()

        return context


class HomePageCarouselImage(Orderable):
    page = ParentalKey(HomePage, on_delete=models.CASCADE, related_name='carousel_images')
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+'
    )
    panels = [
        FieldPanel('image'),
    ]

class Sections(Orderable):
    page = ParentalKey(HomePage, on_delete=models.CASCADE, related_name='sections')
    title = models.CharField(max_length=255)
    description = RichTextField()
    url = models.CharField(max_length=255)
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+'
    )

    panels = [
        FieldPanel('title'),
        FieldPanel('description'),
        FieldPanel('image'),
        FieldPanel('url'),
    ]

