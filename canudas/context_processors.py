from wagtail.models import Site

def global_context(request):
    current_site = Site.find_for_request(request)
    if current_site is None:
        return {}

    root_page = current_site.root_page

    return {
        'page_tree': [root_page] + list(root_page.get_children().live().public()),
    }

