# Project Description

This project is a web platform for the Canudas Aeroclub.

It is built using Django, Wagtail and Tailwind, and is designed to provide a rich content management experience for members and visitors. It includes a custom article management system, user authentication, and a contact form with Mailchimp integration.

# Project Setup Instructions

## Installing Dependencies

To install the project dependencies, ensure you have [Poetry](https://python-poetry.org/) installed. If you don't have Poetry, you can install it with this command:

```bash
curl -sSL https://install.python-poetry.org | python3 -
```

Once Poetry is installed, navigate to the project's root directory and run:

```bash
poetry install
```

This will install all the necessary dependencies for the project as specified in the `pyproject.toml` file.

## Setting Up the `.env` File

Before running the project, you need to set up your environment variables. Copy the `.env.example` file to a new file named `.env` and fill in the values:

```bash
cp .env.example .env
```

Open the `.env` file in a text editor and update the variables with your specific settings.

## Preparing the data

Remember to run migrations and collect static files before starting the site in production:

```bash
poetry run python manage.py migrate
poetry run python manage.py collectstatic
```

## Upgrading

To upgrade:

```bash
$ git pull
$ poetry update
$ poetry run python manage.py migrate
$ poetry run python manage.py collectstatic
```

## Starting the Site

### Development Environment

To run the site in the development environment, use the following command:

```bash
poetry run python manage.py runserver
```

This command starts the Django development server, allowing you to access the site at `http://localhost:8000` by default.

Or to use the production-like settings:

```bash
poetry run python manage.py runserver --settings=canudas.settings.production 0.0.0.0:8000
```

### Production Environment

For production, it is recommended to use a WSGI server such as Gunicorn. First, collect static files:

```bash
poetry run python manage.py collectstatic
```

Then, run the site with Gunicorn:

```bash
poetry run gunicorn canudas.wsgi:application
```

