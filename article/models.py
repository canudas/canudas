from django.db import models

from wagtail.admin.panels import FieldPanel, InlinePanel, PageChooserPanel
from wagtail.fields import RichTextField
from wagtail.images.models import Image
from wagtail.models import Orderable, Page
from wagtail.snippets.models import register_snippet
from modelcluster.fields import ParentalKey

class ArticlePage(Page):
    main_image = models.ForeignKey(
        'wagtailimages.Image', 
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    body = RichTextField()

    content_panels = Page.content_panels + [
        FieldPanel('main_image'),
        FieldPanel('body'),
        InlinePanel('gallery_images', label="Gallery Images"),
    ]

    def get_context(self, request):
        context = super().get_context(request)

        context['sibling_articles'] = self.get_siblings().live().exclude(id=self.id).specific()        
        
        return context

class ArticleGalleryImage(Orderable):
    page = ParentalKey(ArticlePage, on_delete=models.CASCADE, related_name='gallery_images')
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+'
    )
    panels = [
        FieldPanel('image'),
    ]

class ArticleIndexPage(Page):
    long_title = models.CharField(max_length=255, blank=True)
    intro = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('long_title'),
        FieldPanel('intro'),
    ]

    def children(self):
        return ArticlePage.objects.descendant_of(self).live().specific()

    def get_context(self, request):
        context = super().get_context(request)
        context['articles'] = self.children()
        return context
